<?php

/**
 * Moneris Payment Controller.
 */
class MonerisPaymentMethodController extends PaymentMethodController {

  public $payment_configuration_form_elements_callback = 'moneris_payment_form';

  public $payment_method_configuration_form_elements_callback = 'moneris_payment_configuration_form';

  protected $type = 'purchase';

  protected $crypt = 7;

  protected $order_id;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->title = t('Moneris');
    // @todo Make this configurable
    $this->order_id = 'webnet-order-' . date('dmy-Gis');
  }

  /**
   * {@inheritdoc}
   */
  public function execute(Payment $payment) {
    $total = 0.00;
    $controller = $payment->method->controller_data;

    foreach ($payment->getLineItems() as $line_item) {
      $total += $line_item->amount * $line_item->quantity;
    }

    // Setup the transaction array.
    $txn = [
      'type' => $this->type,
      'amount' => number_format($total, 2),
      'order_id' => $this->order_id,
      'crypt_type' => $this->crypt,
      'pan' => $payment->method_data['pan'],
      'expdate' => $payment->method_data['expdate']
    ];

    // Setup CVD verification.
    $cvd = [
      'cvd_indicator' => 1,
      'cvd_value' => $payment->method_data['cvd']
    ];

    $mpgTxn = new mpgTransaction($txn);
    $mpgTxn->setCvdInfo(new mpgCvdInfo($cvd));

    $mpgRequest = new mpgRequest($mpgTxn);
    if ($controller['env'] == 'test') {
      $mpgRequest->setTestMode(TRUE);
    }
    else {
      $mpgRequest->setProcCountryCode($controller['env']);
      $mpgRequest->setTestMode(FALSE);
    }

    $mpgHttpPost = new mpgHttpsPost(
      $controller['store_id'],
      $controller['api_token'],
      $mpgRequest
    );

    // @todo Check other response codes/ messages.
    $mpgResponse = $mpgHttpPost->getMpgResponse();
    if (strstr($mpgResponse->responseData['Message'], 'APPROVED')) {
      $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_SUCCESS));
      return TRUE;
    }
    else {
      $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_FAILED));
      return FALSE;
    }
  }

}

